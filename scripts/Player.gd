class_name Player
extends Area2D


@export var speed: float = 4
@export var damage: float = 10
@export var transition_speed: float = 0.05

var floating_text = preload("res://scenes/FloatingText.tscn")

var modulate_tween: Tween = null
var posses_tween :Tween = null
var possesion_positions := [Vector2(0,16),Vector2(16,0),Vector2(0,-16),Vector2(-16,0),
Vector2(16,16),Vector2(16,-16),Vector2(-16,-16),Vector2(-16,16)]

var spawnpoint : Vector2 = Vector2(0,0)
var move_tag : bool = false
var move_tween : Tween = null
signal moved

const TILE_SIZE = 16

const inputs = {
	"Left": Vector2.LEFT,
	"Right": Vector2.RIGHT,
	"Up": Vector2.UP,
	"Down": Vector2.DOWN
}

var last_dir = Vector2.ZERO
var last_action = ""


func _ready():
	PlayerState.player = self
	PlayerState.player_dead.connect(_die)
		
	# align position to the middle of a tile
	position.x = int(position.x / TILE_SIZE) * TILE_SIZE
	position.y = int(position.y / TILE_SIZE) * TILE_SIZE

	position += Vector2.ONE * TILE_SIZE/2
	spawnpoint = position
	# set timer interval according to the speed
	$MoveTimer.wait_time = 1.0/speed


func _die(true_death: bool):
	if PlayerState.stored_npc != null:
		var temp_npc = PlayerState.stored_npc
		deposses()
		temp_npc.get_parent().remove_child(temp_npc)
		temp_npc.queue_free()
		PlayerState.increase_health(PlayerState.MAX_HEALTH)
	else:
		print("You have died.")
		position = spawnpoint
		PlayerState.increase_health(PlayerState.MAX_HEALTH)
	

func _unhandled_input(event):
	for action in inputs:
		if event.is_action_pressed(action):
			var dir = inputs[action]
			if move_tile(dir):
				# repeat the action in fixed intervals, if it is still pressed
				last_action = action
				last_dir = dir
				$MoveTimer.start()

func _on_move_tag():
	move_tag = false

func move_tile(direction: Vector2):
	$RayCast2D.target_position = direction * TILE_SIZE
	$RayCast2D.force_raycast_update()
	if !$RayCast2D.is_colliding() && !move_tag:
		var tween = create_tween()
		var next_position = position + direction * TILE_SIZE
		tween.finished.connect(_on_move_tag)
		move_tag = true
		tween.tween_property(self, "position", next_position, transition_speed).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_BOUNCE)
		
		moved.emit()
		return true
		
	var other = $RayCast2D.get_collider()
	if (other is Collidable):
		other.on_collision(self)
	return false

func _on_MoveTimer_timeout():
	if Input.is_action_pressed(last_action):
		if move_tile(last_dir):  # do the same move as the last time
			return
	# reset
	last_action = ""
	last_dir = Vector2.ZERO
	$MoveTimer.stop()

func get_hurt(value):
	PlayerState.decrease_health(value)
	$Sprite2D.self_modulate = Color(1,0,0,1)
	var text = FloatText.new(str(value))
	add_child(text)
	if (value > 10):
		text.crit_hit(2)
	else:
		text.leaf_like_rise(5)
	await get_tree().create_timer(0.1).timeout
	$Sprite2D.self_modulate = Color(1,1,1,1)

func collect_coins(value):
	PlayerState.add_coins(value)



func _input(event):
	
	if event.is_action_pressed("Posses") && PlayerState.near_npc != null && PlayerState.stored_npc == null:
		posses_tween = create_tween()
		var old_position = position
		move_tag = true
		posses_tween.tween_property(self, "position", PlayerState.near_npc.position, 0.3)
		posses_tween.tween_property(self, "modulate", Color(1,1,1,0), 0.3)
		await posses_tween.finished
		move_tag = false
		position = PlayerState.near_npc.position
		PlayerState.stored_region = $Sprite2D.region_rect
		$Sprite2D.region_rect = PlayerState.near_npc.region_rect
		modulate = Color(1,1,1,1)
		PlayerState.near_npc.get_possesed()
		
		
	elif event.is_action_pressed("DePosses") && PlayerState.stored_npc != null:
		deposses()


func deposses():
	for direction in possesion_positions:
		$PossesionRayCast.target_position = direction
		$PossesionRayCast.force_raycast_update()
		if !$PossesionRayCast.is_colliding():
			$Sprite2D.region_rect = PlayerState.stored_region
			posses_tween = create_tween()
			PlayerState.stored_npc.get_unpossesed(position)
			modulate = Color(1,1,1,0)
			move_tag = true
			posses_tween.tween_property(self, "modulate", Color(1,1,1,1), 0.3)
			posses_tween.tween_property(self, "position", position + direction, 0.3)
			await posses_tween.finished
			move_tag = false
			break

func _on_area_entered(area):
	if area.is_in_group("NPC"):
		PlayerState.near_npc = area.get_parent()


func _on_area_exited(area):
	if area.is_in_group("NPC"):
		PlayerState.near_npc = null
		
