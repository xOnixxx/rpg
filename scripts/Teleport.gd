extends Area2D


@export var next_scene: String = "res://scenes/House.tscn"

var is_active = false

func _ready():
	pass

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		if PlayerState.stored_npc == null:
			PlayerPosition.player_location[get_tree().current_scene.name] = PlayerState.player.position
			TransitionScreen.transition(next_scene)
		else:
			print("You need sunlight to keep possesing somenone.")



func _on_Teleport_area_entered(area):
	if area.is_in_group("Player"):
		is_active = true


func _on_Teleport_area_exited(area):
	if area.is_in_group("Player"):
		is_active = false
