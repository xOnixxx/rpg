extends CanvasLayer


# Called when the node enters the scene tree for the first time.
func _ready():
	$ColorRect.modulate = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func transition(next_scene):
	$AnimationPlayer.play("FadeScene")
	await $AnimationPlayer.animation_finished
	get_tree().change_scene_to_file(next_scene)
	$AnimationPlayer.play_backwards("FadeScene")
	await $AnimationPlayer.animation_finished
