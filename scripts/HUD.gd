extends CanvasLayer

var coin_text := "Coins: %s"
var current_quest := "Current quest: %s"
var hp_delta: float = 0.1
var damaged: bool = false

var health_tween : Tween = null
var coin_tween : Tween = null

@onready var label = get_node("Label")
# Called when the node enters the scene tree for the first time.
func _ready():
	QuestTracker.quest_complete.connect(refresh_quest)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func _on_damaged():
	damaged = false
	
func update_health():
	if health_tween != null && health_tween.is_running():
		health_tween.stop()
	health_tween = create_tween()
	health_tween.tween_property($MainBody, "value", PlayerState.health , 0.2)
	health_tween.tween_property($Decrease, "value", PlayerState.health , 0.7)
	
func update_coins(old_val: int, new_val: int):
	coin_tween = create_tween()
	coin_tween.tween_method(update_coin_step,old_val, new_val,1)

	
func update_coin_step(new_val: int):
	coin_text = coin_text % new_val
	$Coins.text = coin_text
	coin_text = "Coins: %s"

	
func update_quest(quest_info :String):
	current_quest = current_quest % quest_info
	$QuestTracker.text = current_quest
	current_quest = "Current quest: %s"
	
func refresh_quest():
	current_quest = current_quest % "None"
	$QuestTracker.text = current_quest
	current_quest = "Current quest: %s"
