extends Node2D

@export var Interactable := "Player"
@export var AnimationName := "PanelFade"
@export var message :String = "" 


# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasLayer/TextureRect/Label.modulate = 0
	$CanvasLayer/TextureRect.modulate = 0
	$CanvasLayer/TextureRect/Label.text = message
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_area_entered(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play(AnimationName)


func _on_area_exited(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play_backwards(AnimationName)
