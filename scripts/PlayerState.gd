extends Node


const MAX_HEALTH: float = 100
var health: float = 100
var coins: float = 0

var near_npc = null
var stored_npc = null
var stored_region = null
var new_region = null

signal player_ready(player: Player)
signal player_dead(true_death: bool)
signal player_coins(value: int)
signal player_damaged()

var player:
	get:
		return player
	set(value):
		player = value
		player_ready.emit(player)

func increase_health(value: int):
	health = clamp(health + value, 0, MAX_HEALTH)
	HUD.update_health()

func decrease_health(value: int):
	health = clamp(health - value, 0, MAX_HEALTH)
	player_damaged.emit()
	HUD.update_health()
	if health <= 0:
		if PlayerState.stored_npc != null:
			player_dead.emit(false)
		else:
			player_dead.emit(true)

func add_coins(value: int):
	HUD.update_coins(coins, coins+value)
	coins += value
	player_coins.emit(coins)

