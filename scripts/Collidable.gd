class_name Collidable
extends CollisionObject2D

@export var dmg := 10
@export var crit_chance :float= 0.25


func on_collision(other: CollisionObject2D):
	if other is Player:
		var crit = randf()
		if (crit_chance >= crit):
			other.get_hurt(2*dmg)
		else:
			other.get_hurt(dmg)
