extends Node

var player_location := {}

# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerState.player_ready.connect(on_player_ready)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func on_player_ready(player: Player):
	var scene_name = get_tree().current_scene.name
	if player_location.has(scene_name):
		player.position = player_location[scene_name]
		print("Updated Camera")
