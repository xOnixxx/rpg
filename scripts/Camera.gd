extends Camera2D

@export var FocusPoint :Node
@export var NumOfShakes :int = 1
@export var Intensity :int = 1
var shake_tween : Tween = null
# Called when the node enters the scene tree for the first time.
func _ready():
	position = FocusPoint.position
	PlayerState.player_damaged.connect(_on_player_hit)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if FocusPoint != null:
		position = lerp(position, FocusPoint.position, 2*delta)

func _on_player_hit():
	shake_tween = create_tween()
	var original_position = offset
	var new_position : Vector2
	for i in NumOfShakes:
		new_position = Vector2(randf_range(-20,20), randf_range(-20,20))
		shake_tween.tween_property(self, "offset", new_position, 1/(Intensity*2*(((NumOfShakes-i)*(NumOfShakes-i)+0.0)/10)))
	shake_tween.tween_property(self, "offset", original_position, 0.5)
