extends Collidable

func on_collision(other: CollisionObject2D):
	if other is Player:
		other.get_hurt(dmg)
