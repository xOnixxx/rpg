extends Area2D

@export var coin_value := 0
@export var scale_curve: Curve
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var time = Time.get_ticks_msec() + 1
	var x = (time % 1000) / 1000.0
	var scale = scale_curve.sample(x)
	$Sprite2D.scale = Vector2.ONE * scale
	



func _on_area_entered(area):
	if area.is_in_group("Player"):
		PlayerState.add_coins(coin_value)
		var parent = get_parent()
		parent.remove_child(self)
		print(parent)
		queue_free()
