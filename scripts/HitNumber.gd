extends Label
class_name FloatText



# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func _init(text):
	self.text = text
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func crit_hit(duration: int):
	self.modulate = Color(1,0,0,1)
	var move_tween = create_tween()
	var new_position = position + Vector2(0,-100)
	
	move_tween.connect("finished", on_tween_finished)
	var fade_tween = create_tween()
	fade_tween.tween_property(self, "modulate", Color(0.5,0,0,0), duration*0.75)
	
	move_tween.tween_property(self, "position", new_position, duration)

#Could have been done with animation
func leaf_like_rise(duration: int):
	var move_tween = create_tween()
	var fade_tween = create_tween()
	fade_tween.tween_property(self, "modulate", Color(1,1,1,0), (duration+1)*0.5+duration*0.15/2)
	move_tween.connect("finished", on_tween_finished)
	var new_position
	position = position + Vector2((randf()-0.5)*10.0,-20)
	for i in duration:
		new_position = position + Vector2(-10*pow(-1,i+1),-10*(i+1))
		move_tween.tween_property(self, "position", new_position, 0.5+(0.15*i))
	
func on_tween_finished():
	queue_free()
