extends Sprite2D

@export var quest_dialogue :String = "" 
@export var quest_info :String = "" 
@export var quest_id : int = 0

var player_near := false

# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasLayer/TextureRect/Label.text = quest_dialogue
	$CanvasLayer/TextureRect.hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_area_2d_area_entered(area):
	if area.is_in_group("Player"):
		player_near = true
		$CanvasLayer/TextureRect.show()
		_update_text()

func _unhandled_input(event):
	if player_near:
		if event.is_action_pressed("Action") && QuestTracker.completed_quest[quest_id-1] == 1:
			print("You already completed this quest.")
		elif event.is_action_pressed("Action") && QuestTracker.active_quest != quest_id:
			print("Quest accepted.")
			print(quest_id)
			_update_text()
			HUD.update_quest(quest_info)
			QuestTracker.active_quest = quest_id
			PlayerState.add_coins(0)

func _on_area_2d_area_exited(area):
	if area.is_in_group("Player"):
		player_near = false
		$CanvasLayer/TextureRect.hide()

func get_possesed():
	PlayerState.new_region = self.region_rect
	PlayerState.stored_npc = self
	self.position = Vector2(0,0)
	
func get_unpossesed(player_position:Vector2):
	self.position = player_position
	PlayerState.stored_npc = null
	PlayerState.stored_region = null
	PlayerState.near_npc = null

func _too_close(area):
	if area.is_in_group("Player"):
		$CanvasLayer/TextureRect/Label.text = "You are too close, get off me!"


func _leave_close(area):
	if area.is_in_group("Player"):
		_update_text()

func _update_text():
	if QuestTracker.active_quest != quest_id:
		$CanvasLayer/TextureRect/Label.text = quest_dialogue
	elif QuestTracker.completed_quest[quest_id-1] == 0:
		$CanvasLayer/TextureRect/Label.text = "You haven't completed the quest yet."
	elif QuestTracker.completed_quest[quest_id-1] == 1:
		$CanvasLayer/TextureRect/Label.text = "You've completed the quest. Thanks bro."









