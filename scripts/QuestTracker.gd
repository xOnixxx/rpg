extends Node2D

var number_of_quests := 2
var active_quest : int = 0
var completed_quest := []

signal quest_complete()
# Called when the node enters the scene tree for the first time.
func _ready():
	completed_quest.resize(5)
	completed_quest.fill(0)
	PlayerState.player_dead.connect(die_quest)
	PlayerState.player_coins.connect(gold_quest)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

#QuestID 1
func die_quest(true_death: bool):
	if active_quest == 1 && completed_quest[0] != 1 && true_death:
		print("Quest Completed.")
		PlayerState.add_coins(1000)
		completed_quest[0] = 1
		quest_complete.emit()

#QuestID 2
func gold_quest(value: int):
	if active_quest == 2 && completed_quest[1] != 1 && value >= 1500:
		print("Quest Completed.")
		completed_quest[1] = 1
		PlayerState.add_coins(666666)
		quest_complete.emit()
		


