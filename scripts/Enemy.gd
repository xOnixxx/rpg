extends Collidable

@export var MAX_HEALTH: float = 100
var health: float = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	health = MAX_HEALTH

func on_collision(other: CollisionObject2D):
	if other is Player:
		var crit = randf()
		if (crit_chance >= crit):
			other.get_hurt(2*dmg)
		else:
			other.get_hurt(dmg)
		update_health(other.damage)

func update_health(damage: float):
	health -= damage
	if health < 0:
		die()
	
func die():
	print("Aargh")
	var dissapear = create_tween()
	dissapear.connect("finished", _die_frfr)
	dissapear.tween_property(self, "modulate", Color(1,1,1,0), 0.2)
	
func _die_frfr():
	queue_free()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
